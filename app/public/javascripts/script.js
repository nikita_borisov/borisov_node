// Function for adding items to array with tasks

let taskArray = [];
// let newTask;
let doneArray;
let undoneArray;
let body = 'body';
let countTodo;
countTodo = 0;
const n = 5;
let pageAmount;
let currentPageContent = [];
let checkBoxStatus = '';
let checkBoxClass = '';
let pageList = [];
$(document).ready(function () {
    let counter = function () {
        doneArray = _.filter(taskArray, (item) => item.checked === true);
        undoneArray = _.reject(taskArray, (item) => item.checked === true);
    };
    function loadTasks(display = pagination) {
        $.ajax({
            type: "GET",
            url: "/tasks",
            success: function (response) {
                taskArray = response;
                console.log(response);
                counter();
                display();
                if(doneArray.length === taskArray.length && doneArray.length !== 0){
                    $('.counter').prop('checked', true);
                } else {
                    $('.counter').prop('checked', false);
                }
                $(all).html(taskArray.length);
                $(undone).html(undoneArray.length);
                $(done).html(doneArray.length);
            }
        });
    }
    loadTasks();
    let editText = '.edit_text';
    let taskStatus = '.current-tasks';
    let done = '#done';
    let undone = '#not_done';
    let all = '#counter';
// Function for entering text and adding new task
    $('#submit_event').click(function () {
        const text = $('#input-text').val().trim();
        if (text.length) {
            let item = {
                text: text,
                checked: false,
                id: ''
            };
            countTodo++;
            $.ajax({
                type: "POST",
                url: "/tasks",
                data:  {data: item},
                success: function(response){
                    console.log('Sent!', response);
                    item.id = response._id;
                    taskArray.push(response);
                    console.log(taskArray);
                    const newTask = '<li id="' + response._id + '" class="todo_task"><input type="checkbox" class="check" id="check' + response._id + '"><span class="todo_text" id="todo_text' + response._id + '">' + text + '</span><input type="submit" value="delete" class="delete"></li>';
                    $('.main__content').append(newTask);
                    loadTasks();
                },
                error: function () {
                    console.log('Error!');
                }
            });
            counter();

        }
        else {
            alert('You have entered nothing! Try again!');
        }

        $('.form__input-text').val('');
    });
    $('.form__input-text').keyup(function () {
        if (event.keyCode === 13) {
            $('#submit_event').click();
            $(this).val('');
        }
    });
// Checking if all tasks are completed
    function counting() {
        counter();
        $(undone).html(undoneArray.length);
        $(done).html(doneArray.length);
        if (doneArray.length === taskArray.length && doneArray.length > 0) {
            $("#count").prop('checked', true);
        }
        else {
            $("#count").prop('checked', false);
        }
    }

// Marking tasks as checked or not
    function checkingDone(id) {
        $('#' + id).addClass('completed');
    }

    function checkingUndone(id) {
        $('#' + id).removeClass('completed');
    }

// Function that put ticket to checked task
    $(body).on("click", ".check", function () {
        let getId = $(this).parent().attr('id');
        if ($('#check' + getId).is(':checked')) {
            $.ajax({
                type: "PUT",
                url: '/tasks/' + getId,
                // data: {id: getId},
                success: function (response) {
                    console.log(response);
                    checkingDone(getId);
                    _.each(taskArray, function (item) {
                        if(item.id == getId){
                            item.checked = true;
                        }
                    });
                    checkingUndone(getId);
                    loadTasks(selectingPage);
                }
            });
        }
        else {
            $.ajax({
                type: "PUT",
                url: '/tasks/check/' + getId,
                // data: {id: getId},
                success: function (response) {
                    console.log(response);
                    checkingUndone(getId);
                    _.each(taskArray, function (item) {
                        if(item.id == getId){
                            item.checked = false;
                        }
                    });
                    // counting();
                    loadTasks(selectingPage);
                }
            });
        }
    });
    function selectingPage() {
        let getIdPage = parseInt($('.active').attr('id'));
        if ($(taskStatus).hasClass('done')) {
            pagination(doneArray, getIdPage, '.done');
            if (getIdPage > pageAmount) {
                pagination(doneArray, getIdPage - 1, '.done');
            }
        }
        else if ($(taskStatus).hasClass('not-done')) {
            pagination(undoneArray, getIdPage, '.not-done');
            if (getIdPage > pageAmount) {
                pagination(undoneArray, getIdPage - 1, '.not-done');
            }
        }
        else if ($(taskStatus).hasClass('total-amount')) {
            pagination(taskArray, getIdPage);
            if (getIdPage > pageAmount) {
                pagination(taskArray, getIdPage - 1, '.total-amount');
            }
        }
    }

// Checking all tasks
    $(body).on("click", '.counter', function () {
        if ($('#count').is(':checked')) {

            $.ajax({
                type: "PUT",
                url: "/tasks/check_all",
                success: function () {
                    _.map(taskArray, (item) => item.checked = true);
                    counter();
                    $(done).html(doneArray.length);
                    $(undone).html(undoneArray.length);
                    checkingDone();
                    selectingPage();
                    // loadTasks(selectingPage);
                }
            });
        }
        else {
            $.ajax({
                type: "PUT",
                url: "/tasks/uncheck_all",
                success: function () {
                    _.map(taskArray, (item) => item.checked = false);
                    counter();
                    $(done).html(0);
                    $(undone).html(taskArray.length);
                    undoneArray = _.reject(taskArray, (item) => item.checked === true);
                    checkingUndone();
                    // pagination(taskArray);
                    selectingPage();
                }
            });
        }
    });
    $(body).on('dblclick', '.todo_text', function () {
        let getId = $(this).parent().attr('id');
        $(this).after('<input class="text_edited" type="submit">');

        // _.each(taskArray, function (item) {
        //     if(item.id == getId){
        //         itemText = $('#' + getId + ' > .todo_text').text();
        //     }
        // });
        let itemText = $('#' + getId + ' > .todo_text').text();
        $(this).replaceWith(function () {
            return '<input class="edit_text" autofocus maxlength="22" placeholder="Do not enter more than 22 characters" type="text" value="' + itemText + '">';
        });
        $(editText).focus();
        $(editText).bind('blur keyup', function (event) {
            if (event.type == 'blur' || event.keyCode == '13') {
                submittingText(this, this, '.text_edited');
            }
        });
    });
    function submittingText(target, text, button) {
        let getId = $(target).parent().attr('id');
        let itemText = $(editText).val().trim();
        console.log("$(editText).length", $(editText).length);
        if ($(editText).length != 0 && itemText) {
            $(text).replaceWith(function () {
                return '<span class="todo_text">' + itemText + '</span>';
            });
            $(button).remove();
            $.ajax({
                type: "PUT",
                url: '/tasks/text/' + getId,
                data: {text: itemText},
                success: function (response) {
                    console.log(response);
                    loadTasks(selectingPage);
                }
            });
        }
        else alert('Nothing was entered! Please write something!');
    }

    $(body).on('click', '.text_edited', function () {
        submittingText(this, editText, this);
    });
    function displayAll() {
        pagination();
    }

    $('.total-amount').click(function () {
        displayAll();
    });
    // Show done tasks
    $('.done').click(function () {
        pagination(doneArray, undefined, this);
    });
    // Show not done ones
    $('.not-done').click(function () {
        pagination(undoneArray, undefined, this);
    });
    $(this).on("click", '.delete', function () {
        let getId = $(this).parent().attr('id');
        $.ajax({
            type: "DELETE",
            url: '/tasks/' + getId,
            success: function (response) {
                console.log(response);
                loadTasks(selectingPage);
            }
        });
        $(this).parent().remove();
        taskArray = _.reject(taskArray, (item) => item.id === getId);
        countTodo = 0;
        // _.map(taskArray, (item) => item.id = countTodo++);
        counter();
        $(done).html(doneArray.length);
        $(all).html(taskArray.length);
        $(undone).html(undoneArray.length);
        selectingPage();
        if (taskArray.length === 0) {
            pagination();
            counting();
        }
    });
    function removeAll() {
        taskArray = [];
        counter();
        $(done).html(doneArray.length);
        $(all).html(taskArray.length);
        $(undone).html(undoneArray.length);
        $('#count').prop('checked', false);
        countTodo = 0;
        $('.pagination > :not(li:first)').remove();
        $('.pagination > li:first').addClass('active');
        pagination(taskArray);
    }

    $('#delete-all').click(function () {
        $.ajax({
            type: "DELETE",
            url: "/tasks",
            success: function () {
                removeAll();
            }
        });
    });
    function removeDone() {
        _.each(doneArray, function (item) {
            $('#' + item.id).remove();
        });
        taskArray = _.reject(taskArray, (item) => item.checked === true);
        counter();
        $(done).html(doneArray.length);
        $(all).html(taskArray.length);
        $(undone).html(undoneArray.length);
        $('#count').prop('checked', false);
        countTodo = 0;
        _.map(taskArray, (item) => item.id = countTodo++);
        pagination(taskArray);
    }

    $('#delete').click(function () {
        $.ajax({
            type: "DELETE",
            url: '/tasks/done',
            success: function () {
                // console.log(response);
                removeDone();
                loadTasks();
            }
        });
    });
    $(body).on("click", '.page-number', function () {
        let getId = parseInt($(this).attr('id'));
        if ($(taskStatus).hasClass('done')) {
            pagination(doneArray, getId, '.done');
        }
        else if ($(taskStatus).hasClass('not-done')) {
            pagination(undoneArray, getId, '.not-done');
        }
        else pagination(taskArray, getId);
    });
    function pagination(array = taskArray, currentPage, currentTasks = '.total-amount') {
        $('*').removeClass('current-tasks');
        $(currentTasks).addClass('current-tasks');
        pageList = [];
        pageAmount = Math.ceil(array.length / n);
        for (let i = 1; i <= pageAmount; i++) {
            const newPage = '<li class="page-number" id="' + i + 'page">' + i + '</li>';
            pageList.push(newPage);
            pageList = _.uniq(pageList);
        }
        $('.pagination').html(pageList);
        if (currentPage === undefined) {
            currentPage = pageAmount;
        }
        $('#' + currentPage + 'page').addClass('active');
        currentPageContent = [];
        let pageNum = parseInt($('.active').attr('id'));
        let pageContent = array.slice(n * (pageNum - 1), n * pageNum);
        _.each(pageContent, function (item) {
            if (item.checked === true) {
                checkBoxClass = ' completed';
                checkBoxStatus = 'checked="checked"';
            }
            else {
                checkBoxClass = '';
                checkBoxStatus = '';
            }
            let task = '<li id="' + item._id + '" class="todo_task' + checkBoxClass + '"><input type="checkbox" ' + checkBoxStatus + ' class="check" id="check' + item._id + '"><span class="todo_text" id="todo_text' + item._id + '">' + item.text + '</span><input type="submit" value="delete" class="delete"></li>';
            currentPageContent.push(task);
        });
        $('.main__content').html(currentPageContent);
    }
});
