var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var index = require('./routes/index');
var tasks = require('./routes/tasks');
var mongoose = require('mongoose');
// var MongoClient = require('mongodb').MongoClient;
// var modelTask = require('./routes/database');

var app = express();
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/tasks', tasks);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// MongoClient.connect(config.database.url, { promiseLibrary: Promise }, (err, db) => {
//     if (err) {
//        console.log(`Failed to connect to the database. ${err.stack}`);
//     }
//     app.locals.db = db;
//     app.listen(config.port, () => {
//         console.log(`Node.js app is listening at http://localhost:${config.port}`);
//     });
// });
var db = mongoose.connect('mongodb://localhost/todo', {
    useMongoClient: true
});
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log('Running DB');
});
module.exports = app;
