var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var modelTask = require('./database');

router.post('/', function (req, res, next) {
    var task = req.body.data;
    modelTask.create(task, function (err, todo) {
        if (err) {
            return res.status(500).send('Error!');
        }
        res.status(200).send(todo);
    })
});

router.get('/', function (req, res) {
    modelTask.find(function (err, result) {
        if (err) {
            res.status(500).send('Error!');
        }
        res.status(200).send(result);
    });
});

router.delete('/', function (req, res) {
    modelTask.remove(function (err) {
        if (err) {
            res.status(500).send('Nothing was removed');
        }
        res.status(200).send('All items have been removed form DB');
    });
});

router.delete('/done', function (req, res) {
    modelTask.remove({checked: true}, function (err) {
        if (err) throw err;
        console.log('Removed');
    });
});

router.delete('/:id', function (req, res) {
    var index = req.params.id;
    modelTask.remove({_id: index}, function (err, result) {
        if (err) {
            return res.status(500).send('Nothing was removed');
        }
        res.status(200).send('Removed:  ', result);
    });
});

router.put('/check_all', function (req, res) {
    var query = {checked: false};
    var options = {multi: true};
    modelTask.update(query, {$set: {checked: true}}, options, function (err, num) {
        if (err) {
            return res.status(500).send('Error!');
        }
        res.status(200).send("checked", num);
    });
});

router.put('/uncheck_all', function (req, res) {
    var query = {checked: true};
    var options = {multi: true};
    modelTask.update(query, {$set: {checked: false}}, options, function (err, num) {
        if (err) {
            return res.status(500).send('Error!');
        }
        res.status(200).send("checked", num);
    });
});

router.put('/:id', function (req, res) {
    var query = {_id: req.params.id};
    modelTask.update(query, {$set: {checked: true}}, function (err, num) {
        if (err) {
            return res.status(500).send('Error!');
        }
        res.status(200).send("checked", num);
    });
});

router.put('/check/:id', function (req, res) {
    var query = {_id: req.params.id};
    modelTask.update(query, {$set: {checked: false}}, function (err, num) {
        if (err) {
            return res.status(500).send('Error!');
        }
        res.status(200).send("checked", num);
    });
});
router.put('/text/:id', function (req, res) {
    var query = {_id: req.params.id};
    var textEdited = req.body.text;
    modelTask.update(query, {$set: {text: textEdited}}, function (err, num) {
        if (err) {
            return res.status(500).send('Error!');
        }
        res.status(200).send("Edited", num);
    });
});

module.exports = router;
