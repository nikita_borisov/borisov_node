var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Schema = mongoose.Schema;



var taskSchema = new mongoose.Schema({
    text: String,
    checked: Boolean
});
var modelTask = mongoose.model('modelTask', taskSchema);

module.exports = modelTask;